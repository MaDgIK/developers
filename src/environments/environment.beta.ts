import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';

export let properties: EnvProperties = {
  environment: 'beta',
  dashboard: 'developers',
  useCache: false,
  adminToolsAPIURL: "https://beta.services.openaire.eu/uoa-admin-tools/",
  loginServiceURL: "https://beta.services.openaire.eu/developers-api/",
  developersApiUrl: "https://beta.services.openaire.eu/developers-api/",
  domain: 'https://beta.develop.openaire.eu',
  errorLink: '/error',
  baseLink: '',
  reCaptchaSiteKey: "6LezhVIUAAAAAOb4nHDd87sckLhMXFDcHuKyS76P",
  admins: ["helpdesk@openaire.eu"]
};

