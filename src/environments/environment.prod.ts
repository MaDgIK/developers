import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';

export let properties: EnvProperties = {
  environment: 'production',
  dashboard: 'developers',
  useCache: false,
  adminToolsAPIURL: "https://services.openaire.eu/uoa-admin-tools/",
  loginServiceURL: "https://services.openaire.eu/developers-api/",
  developersApiUrl: "https://services.openaire.eu/developers-api/",
  domain: 'https://develop.openaire.eu',
  errorLink: '/error',
  baseLink: '',
  reCaptchaSiteKey: "6LezhVIUAAAAAOb4nHDd87sckLhMXFDcHuKyS76P",
  admins: ["helpdesk@openaire.eu"]
};

