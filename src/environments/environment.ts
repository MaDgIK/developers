// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';

export let properties: EnvProperties = {
  environment: 'development',
  dashboard: 'developers',
  useCache: false,
  adminToolsAPIURL: "http://duffy.di.uoa.gr:19280/uoa-admin-tools/",
  loginServiceURL: "http://dl170.madgik.di.uoa.gr:19580/developers-api/",
  developersApiUrl: "http://dl170.madgik.di.uoa.gr:19580/developers-api/",
  domain: 'http://mpagasas.di.uoa.gr:5001',
  errorLink: '/error',
  baseLink: '',
  reCaptchaSiteKey: "6LcVtFIUAAAAAB2ac6xYivHxYXKoUvYRPi-6_rLu",
  admins: ["kostis30fylloy@gmail.com", "kiatrop@di.uoa.gr"]
};
