import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {properties} from "../../environments/environment";
import {CustomOptions} from "../openaireLibrary/services/servicesUtils/customOptions.class";

export type AffiliationType = 'Organization' |  'University' | 'SME' | 'Public Sector';

export interface PersonalInfo {
    id: string;
    email: string;
    name: string;
    surname: string;
    affiliation: string;
    affiliationType: string;
    position: string;
}

@Injectable({
    providedIn: 'root'
})
export class PersonalService {


    constructor(private http: HttpClient) {
    }

    public getPersonalInfo() {
        return this.http.get<PersonalInfo>(properties.developersApiUrl + 'personal/my-info', CustomOptions.registryOptions());
    }

    public savePersonalInfo(personalInfo: PersonalInfo) {
        return this.http.post<PersonalInfo>(properties.developersApiUrl + 'personal/save', personalInfo, CustomOptions.registryOptions());
    }
}
