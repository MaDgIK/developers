import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {properties} from "../../environments/environment";
import {CustomOptions} from "../openaireLibrary/services/servicesUtils/customOptions.class";
import {map} from "rxjs/operators";

export type Frequency = 'Daily' | 'Weekly' | 'Monthly';
export type Group = 'Commerce' | 'Education' | 'Health' |'Non profit' | 'Public Sector';

export class RegisteredService {
    id: number;
    clientId: string;
    creationDate: Date;
    keyType: "url" | "value" | null;
    name: string;
    owner: string;
    registrationAccessToken: string;
    url: string;
    description: string;
    frequency: Frequency;
    target: Group[] = [];
    contacts: string[] = [];
}

export class Details {
    clientId: string;
    clientIdIssuedAt: number;
    clientName: string;
    clientSecret: string;
    clientSecretExpiresAt: number;
    contacts: string[];
    grantTypes: string[];
    jwks: {
        keys: [
            {
                alg: string;
                e: string;
                kid: string;
                kty: string;
                n: string
            }
        ];
        valid: boolean
    };
    jwksUri: string;
    logoUri: string;
    policyUri: string;
    redirectUris: string[];
    registrationAccessToken: string;
    registrationClientUri: string;
    scope: string;
    tokenEndpointAuthMethod: string;
    tokenEndpointAuthSigningAlg: string;
}

export class API {
    service: RegisteredService;
    details: Details;
    invalid: boolean = false;

    constructor() {
        this.service = new RegisteredService();
        this.details = new Details();
        this.invalid = false;
    }
}

export class APIsByIssuer {
    issuer: string;
    apis: API[];
}

@Injectable({
    providedIn: 'root'
})
export class ApisService {

    constructor(private http: HttpClient) {
    }

    public getAll(): Observable<APIsByIssuer[]> {
        return this.http.get<APIsByIssuer[]>(properties.developersApiUrl + "apis/all", CustomOptions.registryOptions());
    }

    public copyServices(copyServices: any): Observable<API[]> {
        return this.http.post<API[]>(properties.developersApiUrl + "apis/copy", copyServices, CustomOptions.registryOptions());
    }

    public getMyServices(): Observable<API[]> {
        return this.http.get<API[]>(properties.developersApiUrl + "apis/my-services", CustomOptions.registryOptions())
            .pipe(map(apis => apis.map(api => this.setInvalid(api))));
    }

    public create(form: any): Observable<API> {
        return this.http.post<API>(properties.developersApiUrl + "apis/save/new", form, CustomOptions.registryOptions())
            .pipe(map(api => this.setInvalid(api)));
    }

    public save(id: number, form: any): Observable<API> {
        return this.http.post<API>(properties.developersApiUrl + "apis/save/" + id, form, CustomOptions.registryOptions())
            .pipe(map(api => this.setInvalid(api)));
    }

    public delete(id: number): Observable<void> {
        return this.http.delete<void>(properties.developersApiUrl + "apis/delete/" + id, CustomOptions.registryOptions());
    }

    private setInvalid(api: API) {
        api.invalid = !api.service.url || !api.service.description || !api.service.target || api.service.target.length == 0;
        return api;
    }
}
