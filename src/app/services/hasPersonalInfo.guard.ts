import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {PersonalService} from "./personal.service";
import {catchError, map, tap} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class HasPersonalInfoGuard  {

    constructor(private personalService: PersonalService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.personalService.getPersonalInfo().pipe(catchError(() => of(null)), map(info => !!info), tap(hasInfo => {
            if(!hasInfo) {
                this.router.navigate(['/personal-info'], {skipLocationChange: true, state: {message: true, redirect: state.url}});
            }
        }));
    }

}
