import {Component, OnInit, ViewChild} from "@angular/core";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {NotificationHandler} from "../openaireLibrary/utils/notification-handler";
import {AlertModal} from "../openaireLibrary/utils/modal/alert";

declare var copy;

@Component({
  selector: 'home',
  template: `
    <div page-content>
      <div inner class="uk-container uk-container-small uk-section">
        <div class="uk-alert uk-alert-primary uk-flex uk-flex-middle uk-margin-medium-top">
          <icon name="info" [type]="'outlined'" [flex]="true"></icon>
          <span class="uk-margin-small-left">
            For further information on how to use the tokens please visit the
            <a href="https://graph.openaire.eu/docs/apis/authentication/#personal-access-token" target="_blank">OpenAIRE API Authentication documentation</a>.
        </span>
        </div>
        <div class="uk-margin-large-bottom">
          <h5 class="uk-text-primary">Your personal access token is</h5>
          <div class="clipboard-wrapper uk-width-1-1 uk-flex uk-flex-middle uk-flex-center">
            <div class="uk-width-expand">
            <pre class="uk-margin-remove-bottom uk-flex">
              <code id="accessToken" class="uk-margin-small-left uk-margin-small-top uk-margin-small-bottom">{{accessToken}}</code>
            </pre>
            </div>
            <a class="uk-width-auto uk-margin-small-left uk-margin-xsmall-right" (click)="copyToClipboard('accessToken')">
              <icon name="content_copy"></icon>
            </a>
          </div>
          <div class="uk-flex uk-flex-middle uk-margin-small-top">
            <icon name="info" [type]="'outlined'" [flex]="true"></icon>
            <span class="uk-margin-small-left">Your access token is <span class="uk-text-bold">valid for an hour</span>.</span>
          </div>
          <div class="uk-text-warning uk-flex uk-flex-middle uk-margin-small-top">
            <icon name="warning" [flex]="true"></icon>
            <span class="uk-margin-small-left">Do not share your personal access token. Send your personal access token only over HTTPS.</span>
          </div>
        </div>
        <div *ngIf="!showRefreshToken">
          <h5 class="uk-text-primary">Do you need a refresh token?</h5>
          <div class="uk-flex uk-flex-middle uk-margin-small-top">
            <icon name="info" [type]="'outlined'" [flex]="true"></icon>
            <span class="uk-margin-small-left">OpenAIRE refresh token <span class="uk-text-bold">expires after 1 month</span> and allows you to programmatically get a new access token.</span>
          </div>
          <button (click)="openRefreshTokenModal()" class="uk-button uk-button-primary uk-margin-medium-top">Get a refresh Token</button>
        </div>
        <div *ngIf="showRefreshToken">
          <h5 class="uk-text-primary">Your refresh token is</h5>
          <div class="clipboard-wrapper uk-width-1-1 uk-flex uk-flex-middle uk-flex-center">
            <div class="uk-width-expand">
            <pre class="uk-margin-remove-bottom uk-flex">
              <code id="refreshToken" class="uk-margin-small-left uk-margin-small-top uk-margin-small-bottom">{{refreshToken}}</code>
            </pre>
            </div>
            <a class="uk-width-auto uk-margin-small-left uk-margin-xsmall-right" (click)="copyToClipboard('refreshToken')">
              <icon name="content_copy"></icon>
            </a>
          </div>
          <div class="uk-flex uk-flex-middle uk-margin-small-top">
            <icon name="info" [type]="'outlined'" [flex]="true"></icon>
            <span class="uk-margin-small-left">OpenAIRE refresh token <span class="uk-text-bold">expires after 1 month</span> and allows you to programmatically get a new access token.</span>
          </div>
          <div class="uk-text-warning uk-flex uk-flex-middle uk-margin-small-top">
            <icon name="warning" [flex]="true"></icon>
            <span class="uk-margin-small-left">Please copy your refresh token and store it confidentially. You will not be able to retrieve it. 
              Do not share your refresh token. Send your refresh token only over HTTPS.</span>
          </div>
        </div>
      </div>
    </div>
    <modal-alert #refreshTokenModal (alertOutput)="getRefreshToken()" [overflowBody]="false"></modal-alert>
  `
})
export class PersonalTokenComponent implements OnInit {
  accessToken: string;
  refreshToken: string;
  showRefreshToken: boolean = false;
  subscriptions: any[] = [];
  @ViewChild("refreshTokenModal") modal: AlertModal;

  constructor(private userManagementService: UserManagementService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.accessToken = user.accessToken;
      this.refreshToken = user.refreshToken;
    }))
  }

  getRefreshToken() {
    this.showRefreshToken = true;
  }

  copyToClipboard(id: string) {
    if(copy.exec(id)) {
      NotificationHandler.rise('Copied to clipboard!');
    } else {
      NotificationHandler.rise('Unable to copy to clipboard!', 'danger');
    }
  }

  openRefreshTokenModal() {
    this.modal.alertTitle = 'Get refresh token';
    this.modal.message = 'In case you already have a refresh token, it will no longer be valid. Do you want to proceed?';
    this.modal.okButtonText = 'Get Refresh Token';
    this.modal.okButtonLeft = false;
    this.modal.open();
  }
}
