import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {PersonalTokenComponent} from "./personal-token.component";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {HasPersonalInfoGuard} from "../services/hasPersonalInfo.guard";

@NgModule({
  declarations: [PersonalTokenComponent],
    imports: [CommonModule, RouterModule.forChild([
        {path: '', component: PersonalTokenComponent, canActivate: [LoginGuard, HasPersonalInfoGuard], canDeactivate: [PreviousRouteRecorder]}
    ]), IconsModule, PageContentModule, AlertModalModule],
  exports: [PersonalTokenComponent]
})
export class PersonalTokenModule {

}
