import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ErrorPageComponent} from "./error/errorPage.component";
import {ErrorModule} from "./openaireLibrary/error/error.module";
import {SharedModule} from "./openaireLibrary/shared/shared.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoadingModule} from "./openaireLibrary/utils/loading/loading.module";
import {SideBarModule} from "./openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";
import {NavigationBarModule} from "./openaireLibrary/sharedComponents/navigationBar.module";
import {HttpClientModule} from "@angular/common/http";
import {QuickContactModule} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.module';
import {AlertModalModule} from "./openaireLibrary/utils/modal/alertModal.module";

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserAnimationsModule,
    LoadingModule,
    HttpClientModule,
    SideBarModule,
    BrowserModule,
    AppRoutingModule,
    ErrorModule,
    SharedModule,
    NavigationBarModule,
    QuickContactModule,
    AlertModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
