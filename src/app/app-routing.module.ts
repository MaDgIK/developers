import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ErrorPageComponent} from "./error/errorPage.component";
import {AdminLoginGuard} from "./openaireLibrary/login/adminLoginGuard.guard";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    data: {hasSidebar: false}
  },
  {
    path: 'personal-info',
    loadChildren: () => import('./personal-info/personal-info.module').then(m => m.PersonalInfoModule)
  },
  {
    path: 'personal-token',
    loadChildren: () => import('./personal-token/personal-token.module').then(m => m.PersonalTokenModule)
  },
  {
    path: 'apis',
    loadChildren: () => import('./apis/apis.module').then(m => m.ApisModule)
  },
  {
    path: 'user-info',
    loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule),
    data: {hasSidebar: false}
  },
  {
    path: 'reload',
    loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule),
    data: {hasSidebar: false, hasHeader: false}
  },
  {
    path: 'theme',
    loadChildren: () => import('./openaireLibrary/utils/theme/theme.module').then(m => m.ThemeModule),
    canActivateChild: [AdminLoginGuard],
    data: {hasSidebar: false, hasHeader: false}
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    canActivateChild: [AdminLoginGuard]
  },
  {
    path: '**',
    pathMatch: 'full',
    component: ErrorPageComponent,
    data: {hasSidebar: false}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
