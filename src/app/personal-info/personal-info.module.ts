import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PersonalInfoComponent} from "./personal-info.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";

@NgModule({
    imports: [CommonModule, RouterModule.forChild([
        {path: '', component: PersonalInfoComponent, canActivate: [LoginGuard], canDeactivate: [PreviousRouteRecorder]}
    ]), PageContentModule, InputModule, LoadingModule],
    declarations: [PersonalInfoComponent],
    exports: [PersonalInfoComponent]
})
export class PersonalInfoModule {

}
