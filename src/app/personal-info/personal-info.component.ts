import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {User} from "../openaireLibrary/login/utils/helper.class";
import {PersonalInfo, PersonalService} from "../services/personal.service";
import {NotificationHandler} from "../openaireLibrary/utils/notification-handler";
import {NavigationEnd, Router} from "@angular/router";
import {EmailService} from "../openaireLibrary/utils/email/email.service";
import {Email} from "../openaireLibrary/utils/email/email";
import {properties} from "../../environments/environment";
import {Composer} from "../openaireLibrary/utils/email/composer";

@Component({
    selector: `personal-form`,
    template: `
        <div page-content>
            <div inner class="uk-section uk-container uk-container-small">
                <div *ngIf="loading" class="uk-height-viewport uk-position-relative">
                    <loading class="uk-position-center"></loading>
                </div>
                <ng-container *ngIf="form">
                    <div *ngIf="message" class="uk-alert uk-alert-warning uk-margin-medium-bottom uk-text-center">
                        In order to visit Personal Token and Registered Services you have to fill the personal info form below. In case you already have registered services, they are still active but you cannot edit them.
                    </div>
                    <div class="uk-text-center uk-margin-large-bottom">
                        <h3>Personal Info</h3>
                    </div>
                    <div class="uk-grid" uk-grid>
                        <div class="uk-width-1-2@m uk-width-1-1" input [formInput]="form.get('name')"
                             [placeholder]="'Name'"></div>
                        <div class="uk-width-1-2@m uk-width-1-1" input [formInput]="form.get('surname')"
                             [placeholder]="'Surname'"></div>
                        <div class="uk-width-1-2@m uk-width-1-1" input [formInput]="form.get('email')" [placeholder]="'Email'"></div>
                        <div class="uk-width-1-2@m uk-width-1-1" input [formInput]="form.get('affiliationType')"
                             [placeholder]="'Affiliation Type'" [type]="'select'" [options]="affiliationTypeOptions"></div>
                        <div class="uk-width-1-2@m uk-width-1-1" input [formInput]="form.get('affiliation')"
                             [placeholder]="'Affiliation'"></div>
                        <div class="uk-width-1-2@m uk-width-1-1" input [formInput]="form.get('position')"
                             [placeholder]="'Position'"></div>
                    </div>
                    <div class="uk-margin-top uk-text-small uk-text-italic">
                        Please note that by submitting your information in this form, you agree to be added to the Graph client mailing list. This will ensure you stay updated with relevant news. If you wish to opt-out, you can do so at any time by contacting the list admins at <a href="mailto:gbikas@openaire.eu">gbikas@openaire.eu</a> and <a href="mailto:stefania.amodeo@openaire.eu">stefania.amodeo@openaire.eu</a>.
                    </div>
                    <div class="uk-flex uk-flex-right@l uk-flex-center uk-margin-medium-top">
                        <a class="uk-button uk-button-default uk-margin-small-right" routerLink="/">Cancel</a>
                        <button class="uk-button uk-button-primary" [disabled]="form.pristine || form.invalid || form.disabled"
                                [class.uk-disabled]="form.pristine || form.invalid || form.disabled" (click)="save()">Save
                        </button>
                    </div>
                </ng-container>
            </div>
        </div>
    `
})
export class PersonalInfoComponent implements OnInit, OnDestroy {
    form: FormGroup;
    loading: boolean = true;
    subscriptions: any[] = [];
    user: User;
    info: PersonalInfo = null;
    affiliationTypeOptions: string[] = ['Organization', 'University', 'SME', 'Public Sector'];
    message: boolean = false;
    redirect: string;

    constructor(private fb: FormBuilder,
                private personalService: PersonalService,
                private userManagementService: UserManagementService,
                private emailService: EmailService,
                private router: Router) {
        this.subscriptions.push(this.router.events.subscribe(event => {
            if(event instanceof NavigationEnd) {
                const navigation = this.router.getCurrentNavigation();
                if (navigation && navigation.extras.state) {
                    this.message = navigation.extras.state.message;
                    this.redirect = navigation.extras.state.redirect;
                } else {
                    this.message = false;
                }
            }
        }));
    }

    ngOnInit() {
        this.subscriptions.push(this.personalService.getPersonalInfo().subscribe(info => {
            this.info = info;
            this.initForm();
            this.message = false;
        }, error => {
            this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
                this.user = user;
                this.initForm();
            }));
        }));
    }

    initForm() {
        if(this.info) {
            this.form = this.fb.group({
                name: this.fb.control({value: this.info.name, disabled: true}, Validators.required),
                surname: this.fb.control({value: this.info.surname, disabled: true}, Validators.required),
                email: this.fb.control({value: this.info.email, disabled: true}, Validators.required),
                affiliation: this.fb.control(this.info.affiliation, Validators.required),
                affiliationType: this.fb.control(this.info.affiliationType, Validators.required),
                position: this.fb.control(this.info.position, Validators.required),
            });
        } else {
            this.form = this.fb.group({
                name: this.fb.control({value: this.user.firstname, disabled: true}, Validators.required),
                surname: this.fb.control({value: this.user.lastname, disabled: true}, Validators.required),
                email: this.fb.control({value: this.user.email, disabled: true}, Validators.required),
                affiliation: this.fb.control('', Validators.required),
                affiliationType: this.fb.control(null, Validators.required),
                position: this.fb.control('', Validators.required),
            });
        }
        this.form.markAsPristine();
        this.loading = false;
    }

    save() {
        this.loading = true;
        this.subscriptions.push(this.personalService.savePersonalInfo(this.form.getRawValue()).subscribe(info => {
            let callback = (info: PersonalInfo) => {
                this.info = info;
                NotificationHandler.rise('Your personal info has been saved successfully.');
                this.initForm();
                this.loading = false;
                this.message = false;
                if (this.redirect) {
                    this.router.navigate([this.redirect]);
                }
            }
            if(!this.info) {
                this.sendEmail(callback, info);
            } else {
                callback(info);
            }
        }, error => {
            console.error(error);
            NotificationHandler.rise('An error has occurred. Please try again later.', 'danger');
            this.initForm();
            this.loading = false;
        }))
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => {
            if(subscription instanceof Subscription) {
                subscription.unsubscribe();
            }
        })
    }

    sendEmail(callback: (info: PersonalInfo) => void, info: PersonalInfo) {
        let email = Composer.composeEmailForDevelopPersonalInfo(this.user);
        this.emailService.sendEmail(properties, email).subscribe( (res) => {
            callback(info);
            this.loading = false;
        }, error => {
            NotificationHandler.rise('An error has occurred. Please try again later!', 'danger');
            this.loading = false;
        })
    }
}
