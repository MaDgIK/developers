import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ApisComponent} from "./apis.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {HasPersonalInfoGuard} from "../services/hasPersonalInfo.guard";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [CommonModule, RouterModule.forChild([
        {
            path: '',
            component: ApisComponent,
            canActivate: [LoginGuard, HasPersonalInfoGuard],
            canDeactivate: [PreviousRouteRecorder]
        }
    ]), PageContentModule, InputModule, LoadingModule, IconsModule, AlertModalModule, FormsModule],
    declarations: [ApisComponent],
    exports: [ApisComponent]
})
export class ApisModule {

}
