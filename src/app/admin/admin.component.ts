import {Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {API, APIsByIssuer, ApisService} from "../services/apis.service";
import {Subscription} from "rxjs";
import {AlertModal} from "../openaireLibrary/utils/modal/alert";
import {NotificationHandler} from "../openaireLibrary/utils/notification-handler";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {Option} from "../openaireLibrary/sharedComponents/input/input.component";
import {FullScreenModalComponent} from "../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.component";
import {EmailService} from "../openaireLibrary/utils/email/email.service";
import {Email} from "../openaireLibrary/utils/email/email";
import {properties} from "../../environments/environment";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Component({
  selector: 'admin',
  template: `
      <div page-content>
          <div header class="uk-section-small uk-container uk-container-small uk-flex uk-flex-right@m uk-flex-center">
              <button (click)="openCopyModal()" 
                      [disabled]="availableIssuers.length === 0"
                      [attr.uk-tooltip]="availableIssuers.length === 0?'There are not available issuers in order to copy from':null"
                      class="uk-button uk-button-primary uk-flex uk-flex-middle">
                  <icon name="add" [flex]="true"></icon>
                  <span class="uk-margin-xsmall-left">Copy Services</span>
              </button>
              <button (click)="openSendEmailModal()" [disabled]="!issuer || recipients.length === 0" class="uk-button uk-button-default uk-margin-small-left uk-flex uk-flex-middle">
                  <icon name="mail" [flex]="true"></icon>
                  <span class="uk-margin-xsmall-left">Send Email</span>
              </button>
          </div>
          <div inner class="uk-section-small uk-container uk-container-small">
              <div *ngIf="loading" class="uk-height-large uk-position-relative">
                  <loading class="uk-position-center"></loading>
              </div>
              <div *ngIf="!loading">
                  <div class="uk-flex uk-flex-middle uk-flex-wrap uk-margin-medium-bottom">
                      <div *ngIf="apisByIssuers.length > 0" class="uk-width-1-2@m">
                          <div input type="select" placeholder="Issuer"  [options]="issuers" [value]="issuer" (valueChange)="changeIssuer($event)"></div>
                      </div>
                      <div class="uk-width-1-2@m">
                          <paging-no-load [currentPage]="page" customClasses="uk-flex-right"
                                          [totalResults]="apis.length" (pageChange)="page = $event.value"></paging-no-load>
                      </div>
                  </div>
                  <div *ngIf="apis.length === 0"
                       class="uk-margin-large-top uk-card uk-card-default uk-height-small uk-position-relative">
                      <div class="uk-position-center uk-text-bold">There are not registered services yet</div>
                  </div>
                  <table *ngIf="apis.length > 0" class="uk-table uk-table-divider uk-margin-medium-top">
                      <thead>
                      <tr>
                          <th>Name</th>
                          <th>Client ID</th>
                          <th>Owner</th>
                          <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr *ngFor="let api of apis.slice((page-1)*10, (page)*10 - 1); let i=index">
                          <td>{{api.service.name}}</td>
                          <td>{{api.service.clientId}}</td>
                          <td>{{api.service.owner}}</td>
                          <td>
                        <span class="uk-flex uk-flex-middle">
                            <icon name="delete" class="clickable uk-margin-xsmall-left"
                                  customClass="uk-text-danger" (click)="openDeleteModal(i)"></icon>
                            <icon *ngIf="api.details" name="info" class="clickable uk-margin-xsmall-left"
                                  customClass="uk-text-secondary" (click)="openInfoModal(i)"></icon>
                        </span>
                          </td>
                      </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      <modal-alert #copyModal (alertOutput)="copy()" [okDisabled]="copyServicesForm?.invalid">
          <div *ngIf="copyServicesForm" class="uk-grid uk-child-width-1-1" uk-grid>
              <div input type="select" placeholder="Issuer" [options]="availableIssuers" [formInput]="copyServicesForm.get('issuer')"></div>
              <div input type="textarea" rows="6" placeholder="Services Json" [formInput]="copyServicesForm.get('services')"></div>
          </div>
      </modal-alert>
      <modal-alert #deleteModal [overflowBody]="false" (alertOutput)="confirm()"></modal-alert>
      <modal-alert #infoModal [large]="false">
          <div *ngIf="api?.details">
              <ul class="uk-list uk-list-divider">
                  <li>
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Service Name</div>
                          <div class="uk-width-expand uk-text-break">{{api.details.clientName}}</div>
                      </div>
                  </li>
                  <li>
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">
                              <div class="uk-text-bold uk-flex uk-flex-middle">
                                  Client Id
                              </div>
                          </div>
                          <div id="client-id" class="uk-width-expand uk-text-break">{{api.details.clientId}}</div>
                      </div>
                  </li>
                  <li>
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Scope</div>
                          <div class="uk-width-expand uk-text-break">openid</div>
                      </div>
                  </li>
                  <li>
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Grant type</div>
                          <div class="uk-width-expand uk-text-break">{{api.details.grantTypes.join(', ')}}</div>
                      </div>
                  </li>
                  <li *ngIf="api.service.contacts?.length > 0">
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Contacts</div>
                          <div class="uk-width-expand uk-text-break">{{api.service.contacts.join(', ')}}</div>
                      </div>
                  </li>
                  <li *ngIf="!api.service.contacts?.length > 0 && api.details.contacts?.length > 0">
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Contacts</div>
                          <div class="uk-width-expand uk-text-break">{{api.details.contacts.join(', ')}}</div>
                      </div>
                  </li>
                  <ng-template [ngIf]="!api.service.keyType" [ngIfElse]="key">
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">
                              <div class="uk-text-bold uk-flex uk-flex-middle">
                                  Client Secret
                              </div>
                          </div>
                          <div id="client-secret"
                               class="uk-width-expand uk-text-break">{{api.details.clientSecret}}</div>
                      </div>
                      <li>
                          <div class="uk-flex">
                              <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Authentication method
                              </div>
                              <div class="uk-width-expand uk-text-break">Client Secret Basic</div>
                          </div>
                      </li>
                  </ng-template>
                  <ng-template #key>
                      <li>
                          <div class="uk-flex">
                              <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Authentication method
                              </div>
                              <div class="uk-width-expand uk-text-break">Asymmetrically-signed JWT assertion</div>
                          </div>
                      </li>
                      <li>
                          <div class="uk-flex">
                              <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Token Endpoint
                                  Authentication Signing Algorithm
                              </div>
                              <div class="uk-width-expand uk-text-break">RSASSA using SHA-256 hash algorithm</div>
                          </div>
                      </li>
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">
                              <div class="uk-text-bold uk-flex uk-flex-middle">
                                  Public Key
                              </div>
                          </div>
                          <div *ngIf="api.details.jwksUri" id="public-key"
                               class="uk-width-expand uk-text-break">{{api.details.jwksUri}}</div>
                          <div *ngIf="api.details.jwks" id="public-key" class="uk-width-expand uk-text-break">
                              <pre><code class="uk-overflow-auto">{{api.details.jwks.keys[0] | json}}</code></pre>
                          </div>
                      </div>
                  </ng-template>
                  <li>
                      <div class="uk-flex">
                          <div class="uk-text-bold uk-width-small uk-margin-xsmall-right">Creation Date</div>
                          <div class="uk-width-expand uk-text-break">{{api.service.creationDate | date: 'dd-MM-YYYY HH:mm'}}</div>
                      </div>
                  </li>
              </ul>
          </div>
      </modal-alert>
      <fs-modal #sendEmailModal [okButtonDisabled]="!emailForm?.valid" (okEmitter)="sendEmail()">
          <div *ngIf="emailForm" class="uk-section uk-container uk-container-small">
              <div class="uk-text-small uk-margin-bottom">
                  <label class="uk-text-bold">Recipients: </label><span [innerHTML]="getRecipientsAsString()"></span>
              </div>
              <form [formGroup]="emailForm">
                  <div input [formInput]="emailForm.get('subject')" class="uk-margin-bottom" placeholder="Subject"></div>
                  <label class="uk-text-bold">Message</label>
                  <ckeditor #ckEditor [readonly]="false" debounce="500"
                            [formControl]="emailForm.get('body')"
                            [config]="{ extraAllowedContent: '* [uk-*](*) ; span', disallowedContent: 'script; *[on*]',
                                  removeButtons:  'Save,NewPage,DocProps,Preview,Print,' +
                                                  'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,' +
                                                  'CreateDiv,Flash,PageBreak,' +
                                                  'Subscript,Superscript,Anchor,Smiley,Iframe,Styles,Font,About,Language',
                                  extraPlugins: 'divarea', height: 500}">
                  </ckeditor>
              </form>
          </div>
      </fs-modal>
  `
})
export class AdminComponent implements OnInit, OnDestroy{
  loading: boolean = true;
  subscriptions: any[] = [];
  apisByIssuers: APIsByIssuer[] = [];
  apis: API[] = [];
  api: API;
  activeIssuer: string;
  issuer: string;
  issuers: Option[] = [];
  availableIssuers: Option[] = [];
  index: number = -1;
  activeIssuerIndex = 0;
  page = 1;
  copyServicesForm: FormGroup;
  emailForm: FormGroup;
  recipients: string[] = [];
  @ViewChild("copyModal") copyModal: AlertModal;
  @ViewChild("deleteModal") deleteModal: AlertModal;
  @ViewChild("infoModal") infoModal: AlertModal;
  @ViewChild("sendEmailModal") sendEmailModal: FullScreenModalComponent;

  constructor(private apisService: ApisService,
              private emailService: EmailService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.apisService.getAll().subscribe(apisByIssuers => {
      this.apisByIssuers = apisByIssuers;
      if(apisByIssuers.length > 0) {
        this.activeIssuer = apisByIssuers[0].issuer;
        this.issuer = this.activeIssuer;
        this.apis = apisByIssuers[0].apis;
        this.setRecipients();
      }
      this.issuers = apisByIssuers.map(api => {
        return {
          label: api.issuer,
          value: api.issuer,
          tooltip: '<span class="uk-text-break">' + api.issuer + '</span>'
        }
      });
      this.availableIssuers = this.issuers.filter(option => option.value != this.activeIssuer);
      this.loading = false;
    }, error => {
      this.apis = [];
      this.loading = false;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if(subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    })
  }

  setRecipients() {
    this.recipients = this.apis.map(api => api.service?.contacts?api.service.contacts:(api.details?.contacts?api.details.contacts:[])).flat();
    /* Remove duplicates */
    this.recipients = this.recipients.filter((email, index) => this.recipients.indexOf(email) === index);
  }

  getRecipientsAsString() {
    if(this.recipients.length > 8) {
      return this.recipients.slice(0, 7).join(', ') + ' <span class="uk-text-bold">+' + (this.recipients.length - 8).toString() + ' more </span>';
    } else {
      return this.recipients.join(', ');
    }
  }

  openCopyModal() {
    this.copyModal.alertTitle = 'Copy Service from another Issuer';
    this.copyModal.okButtonText = 'Copy';
    this.copyModal.okButtonLeft = false;
    this.copyServicesForm = this.fb.group({
      issuer: this.fb.control('', [Validators.required, StringUtils.urlValidator()]),
      services: this.fb.control('', [Validators.required, StringUtils.jsonValidator('The format should be: [{"client_id": ..., "registration_access_token": ...}]')])
    })
    this.copyModal.open();
  }

  copy() {
    this.loading = true;
    let copyServices = {issuer: this.copyServicesForm.get('issuer').getRawValue(), services: JSON.parse(this.copyServicesForm.get('services').getRawValue())};
    this.subscriptions.push(this.apisService.copyServices(copyServices).subscribe(apis => {
      this.apisByIssuers[0].apis = apis;
      this.changeIssuer(this.activeIssuer);
      NotificationHandler.rise('Services has been copied successfully to ' + '<span class="uk-text-break">' + this.activeIssuer + '<span>');
      this.loading = false;
    }, error => {
      console.error(error);
      NotificationHandler.rise('An error has occurred. Please try again later.', 'danger');
      this.loading = false;
    }));
  }

  changeIssuer(issuer) {
    this.issuer = issuer;
    let apisByIssuer = this.apisByIssuers.find(apisByIssuer => apisByIssuer.issuer == issuer);
    this.apis = apisByIssuer.apis;
    this.setRecipients();
    this.page = 1;
  }

  openDeleteModal(index: number) {
    this.index = index;
    let api = this.apis[this.index];
    this.deleteModal.alertTitle = 'Delete Service';
    this.deleteModal.alertMessage = true;
    this.deleteModal.message = 'You are going to delete ' + api.service.name + '. Are you sure you want to proceed?';
    this.deleteModal.okButtonText = 'Yes';
    this.deleteModal.cancelButtonText = 'No';
    this.deleteModal.open();
  }

  openInfoModal(index: number) {
    this.api = this.apis[index];
    this.infoModal.okButton = false;
    this.infoModal.cancelButtonText = 'Close';
    this.infoModal.alertTitle = 'Details for ' + this.api.details.clientName;
    this.infoModal.open();
  }

  confirm() {
    this.loading = true;
    this.subscriptions.push(this.apisService.delete(this.apis[this.index].service.id).subscribe(() => {
      this.apis.splice(this.index, 1);
      this.apisByIssuers[this.activeIssuerIndex].apis = this.apis;
      this.setRecipients();
      NotificationHandler.rise('Your service has been created deleted.');
      this.loading = false;
    }, error => {
      console.error(error);
      NotificationHandler.rise('An error has occurred. Please try again later.', 'danger');
      this.loading = false;
    }));
  }

  openSendEmailModal() {
    this.emailForm = this.fb.group({
      subject: this.fb.control('[OpenAIRE Develop]: ', Validators.required),
      body: this.fb.control('', Validators.required)
    });
    this.sendEmailModal.title = 'Email Sender';
    this.sendEmailModal.okButton = true;
    this.sendEmailModal.back = true;
    this.sendEmailModal.okButtonText = 'Send';
    this.sendEmailModal.open();
  }

  sendEmail() {
    this.loading = true;
    let email = new Email();
    email.body = this.emailForm.get('body').value;
    email.subject = this.emailForm.get('subject').value;
    email.recipients = this.recipients;
    this.emailService.sendEmail(properties, email).subscribe( (res) => {
      if(res) {
        NotificationHandler.rise('Message has been sent!');
      } else {
        NotificationHandler.rise('An error has occurred. Please try again later!', 'danger');
      }
      this.loading = false;
    }, error => {
      NotificationHandler.rise('An error has occurred. Please try again later!', 'danger');
      this.loading = false;
    })
  }
}
