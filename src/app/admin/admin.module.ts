import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {AdminComponent} from "./admin.component";
import {NgModule} from "@angular/core";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {PagingModule} from "../openaireLibrary/utils/paging.module";
import {FullScreenModalModule} from "../openaireLibrary/utils/modal/full-screen-modal/full-screen-modal.module";
import {ReactiveFormsModule} from "@angular/forms";
import {CKEditorModule} from "ng2-ckeditor";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: AdminComponent}
  ]), IconsModule, LoadingModule, PageContentModule, AlertModalModule, InputModule, PagingModule, FullScreenModalModule, ReactiveFormsModule, CKEditorModule],
  declarations: [AdminComponent],
  exports: [AdminComponent]
})
export class AdminModule {}
