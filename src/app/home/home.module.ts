import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {HomeComponent} from "./home.component";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: HomeComponent, canDeactivate: [PreviousRouteRecorder]}
  ]), IconsModule],
  exports: [HomeComponent]
})
export class HomeModule {

}
