import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'home',
  template: `
    <div class="uk-container uk-container-small uk-section uk-text-center">
      <h2>OpenAIRE APIs Authentication</h2>
      <div class="uk-margin-large-top">The OpenAIRE APIs can be accessed over HTTPS both by authenticated and unauthenticated requests. To achieve <span class="uk-text-bold">better rate limits</span> you need to make <span class="uk-text-bold">authenticated requests.</span></div>
      <div class="uk-alert uk-alert-primary uk-flex uk-flex-middle uk-margin-medium-top">
        <icon name="info" [type]="'outlined'" [flex]="true"></icon>
        <span class="uk-margin-small-left">For more information please read the <a href="https://graph.openaire.eu/docs/apis/authentication" target="_blank">OpenAIRE API Authentication documentation</a>.</span>
      </div>
      <div class="uk-grid uk-grid-large uk-child-width-1-2@m uk-child-width-1-1 uk-margin-large-top" uk-grid>
        <div>
          <a routerLink="personal-token" class="uk-card uk-card-default uk-card-hover uk-link-reset uk-card-body uk-flex uk-flex-column uk-flex-center">
            <h6>Personal Token</h6>
            <div>
              Get access to the OpenAIRE APIs with your personal access and refresh token.
            </div>
          </a>
        </div>
        <div>
          <a routerLink="apis" class="uk-card uk-card-default uk-card-hover uk-link-reset uk-card-body uk-flex uk-flex-column uk-flex-center">
            <h6>Registered Services</h6>
            <div>
              Register your services to get access to the OpenAIRE APIs.
            </div>
          </a>
        </div>
      </div>
    </div>
  `
})
export class HomeComponent implements OnInit {
  
  ngOnInit() {
  }
}
