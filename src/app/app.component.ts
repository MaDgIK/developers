import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EnvProperties} from "./openaireLibrary/utils/properties/env-properties";
import {properties} from "../environments/environment";
import {LayoutService} from "./openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {SmoothScroll} from "./openaireLibrary/utils/smooth-scroll";
import {UserManagementService} from "./openaireLibrary/services/user-management.service";
import {Subscriber} from "rxjs";
import {Session, User} from "./openaireLibrary/login/utils/helper.class";
import {MenuItem} from "./openaireLibrary/sharedComponents/menu";
import {Header} from "./openaireLibrary/sharedComponents/navigationBar.component";
import {QuickContactService} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.service';
import {QuickContactComponent} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.component';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {EmailService} from './openaireLibrary/utils/email/email.service';
import {Composer} from './openaireLibrary/utils/email/composer';
import {AlertModal} from './openaireLibrary/utils/modal/alert';
import {NotificationHandler} from './openaireLibrary/utils/notification-handler';

@Component({
    selector: 'app-root',
    template: `
        <div *ngIf="loading">
            <loading [full]="true"></loading>
        </div>
        <div *ngIf="!loading">
            <div id="modal-container"></div>
            <navbar *ngIf="hasHeader" portal="developers" [header]="menuHeader"
                    [userMenuItems]=userMenuItems [menuItems]="menuItems" [user]="user"></navbar>
            <div class="sidebar_main_swipe uk-flex uk-background-default" [class.sidebar_main_active]="open && hasSidebar"
                 [class.sidebar_mini]="!open && hasSidebar"
                 [class.sidebar_hover]="hover">
                  <dashboard-sidebar *ngIf="hasSidebar" [items]="sideBarItems" [backItem]="backItem"></dashboard-sidebar>
                  <main class="uk-width-1-1">
                      <router-outlet></router-outlet>
                  </main>
            </div>
            <quick-contact #quickContact *ngIf="showQuickContact && contactForm" (sendEmitter)="send($event)"
              [contact]="'Help'" [contactForm]="contactForm" [sending]="sending">
            </quick-contact>
            <modal-alert #modal [overflowBody]="false"></modal-alert>
        </div>
    `
})
export class AppComponent implements OnInit, OnDestroy {
    title = 'OpenAIRE APIs Authentication';
    properties: EnvProperties = properties;
    user: User;
    loading = true;
    hasSidebar: boolean = false;
    hasHeader: boolean = true;
    sideBarItems: MenuItem[] = [];
    menuItems: MenuItem[] = [];
    backItem: MenuItem;
    menuHeader: Header = {
        route: "/",
        url: null,
        title: "OpenAIRE APIs Authentication",
        logoUrl: 'assets/common-assets/common/Logo_Horizontal.png',
        logoSmallUrl: 'assets/common-assets/common/Logo_Small.png',
        position: 'center',
        badge: false
    };
    userMenuItems: MenuItem[] = [];
    private subscriptions: any[] = [];

    /* Contact */
    public showQuickContact: boolean;
    public contactForm: UntypedFormGroup;
    public sending = false;
    @ViewChild('quickContact') quickContact: QuickContactComponent;
    @ViewChild('modal') modal: AlertModal;

    constructor(private cdr: ChangeDetectorRef,
                private smoothScroll: SmoothScroll,
                private layoutService: LayoutService,
                private userManagementService: UserManagementService,
                private quickContactService: QuickContactService,
                private fb: UntypedFormBuilder,
                private emailService: EmailService) {
    }

    ngOnInit() {
        this.subscriptions.push(this.layoutService.hasSidebar.subscribe(hasSidebar => {
            this.hasSidebar = hasSidebar;
            this.cdr.detectChanges();
        }));
        this.subscriptions.push(this.layoutService.hasHeader.subscribe(hasHeader => {
            this.hasHeader = hasHeader;
            this.cdr.detectChanges();
        }));
        this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
            if (user) {
                this.user = user;
            } else if (this.user) {
                this.user = user;
            }
            if(this.user) {
              this.reset();
            }
            this.buildMenu();
            this.layoutService.setOpen(true);
            this.loading = false;
        }));
        this.subscriptions.push(this.quickContactService.isDisplayed.subscribe(display => {
          this.showQuickContact = display;
        }));
    }

    public ngOnDestroy() {
        this.subscriptions.forEach(value => {
            if (value instanceof Subscriber) {
                value.unsubscribe();
            }
        });
        this.userManagementService.clearSubscriptions();
        this.layoutService.clearSubscriptions();
        this.smoothScroll.clearSubscriptions();
    }

    public get open() {
        return this.layoutService.open;
    }

    public get hover() {
        return this.layoutService.hover;
    }

    buildMenu() {
        this.backItem = new MenuItem('overview', 'Home', '', '/', false, [], [], null, {name: 'west'});
        let items: MenuItem[] = [
            new MenuItem('personal-info', 'Personal Info', '', '/personal-info', false, [], [], null, {name: 'person'}),
            new MenuItem('personal-token', 'Personal Token', '', '/personal-token', false, [], [], null, {name: 'verified_user'}),
            new MenuItem('apis', 'Registered Services', '', '/apis', false, [], [], null, {name: 'apps'}),
        ];
        if(Session.isPortalAdministrator(this.user)) {
            items.push(new MenuItem('admin', 'Admin Panel', '', '/admin', false, [], [], null, {name: 'admin_panel_settings'}));
        }
        this.sideBarItems = items;
        this.userMenuItems = items;
        this.hasSidebar = this.hasSidebar && this.sideBarItems.length > 1;
    }

    public send(event) {
      if (event.valid === true) {
        this.sendMail(this.properties.admins);
      }
    }
    
    public reset() {
      if (this.quickContact) {
        this.quickContact.close();
      }
      this.contactForm = this.fb.group({
        subject: this.fb.control('', Validators.required),
        message: this.fb.control('', Validators.required),
        recaptcha: this.fb.control('', Validators.required)
      });
    }

    private sendMail(admins: string[]) {
      this.sending = true;
      this.subscriptions.push(this.emailService.contact(this.properties,
        Composer.composeEmailForDevelop(this.contactForm.value, admins, this.user),
        this.contactForm.value.recaptcha).subscribe(
        res => {
          if (res) {
            this.sending = false;
            this.reset();
            this.modalOpen();
          } else {
            this.handleError('Email <b>sent failed!</b> Please try again.');
          }
        },
        error => {
          this.handleError('Email <b>sent failed!</b> Please try again.', error);
        }
      ));
    }

    public modalOpen() {
      this.modal.okButton = true;
      this.modal.alertTitle = 'Your request has been successfully submitted';
      this.modal.message = 'Our team will respond to your submission soon.';
      this.modal.alertMessage = true;
      this.modal.cancelButton = false;
      this.modal.okButtonLeft = false;
      this.modal.okButtonText = 'OK';
      this.modal.open();
    }

    handleError(message: string, error = null) {
      if (error) {
        console.error(error);
      }
      this.sending = false;
      this.quickContact.close();
      NotificationHandler.rise(message, 'danger');
      this.contactForm.get('recaptcha').setValue('');
    }
}
